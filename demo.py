from PIL import Image
import pytesseract


img_name = './001.en-us.png'
#img_name = './002.zh-cht.png'
#img_name = './003.zh-chi.png'

img = Image.open(img_name)

text = pytesseract.image_to_string(img, lang='eng')
#text = pytesseract.image_to_string(img, lang='chi_tra+eng')
#text = pytesseract.image_to_string(img, lang='eng+chi_tra+chi_sim')

print(text)
